function inSoNguyenTo() {
  var n = document.getElementById("nhap-n").value * 1;
  console.log("n: ", n);
  var result = "2 ";
  // so nguyen to>1 và không chia hết cho [2;b]

  for (var i = 3; i <= n; i++) {
    var b = Math.sqrt(i);
    var lasonguyento = true;

    for (var a = 2; a <= b; a++) {
      // neu tim thay bat ki so uoc nao thi dung dong lap 2
      if (i % a == 0) {
        document.getElementById("result").innerHTML = "Không có số nguyên tố";
        lasonguyento = false;
        break;
      }
    }
    if (lasonguyento) {
      result = result + i + " ";
      console.log(result);
    }
  }

  if (result == "") {
    document.getElementById("result").innerHTML = "Không có số nguyên tố";
  } else {
    document.getElementById("result").innerHTML = `Số nguyên tố ${result}`;
  }
}
